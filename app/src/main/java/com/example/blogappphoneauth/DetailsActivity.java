package com.example.blogappphoneauth;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class DetailsActivity extends AppCompatActivity {

    private TextView activityDetailTitle, activityDetailType, activityDetailDescription, activityDetailStartdate, activityDetailStarttime, activityDetailEnddate, activityDetailEndtime, activityDetailVenue, activityDetailTargetGroup, activityDetailQuota, activityDetailFee, activityDetailRegMethod, activityDetailDeadDate, activityDetailDeadTime;
    private TextView activityDetailPosterImage;
    private Button activityDetailBtn;
    public Context context;

    private TextView activityDetailTypeStatic, activityDetailDescriptionStatic, activityDetailStartdateStatic, activityDetailStarttimeStatic, activityDetailEnddateStatic, activityDetailEndtimeStatic, activityDetailVenueStatic, activityDetailTargetGroupStatic, activityDetailQuotaStatic, activityDetailFeeStatic, activityDetailRegMethodStatic, activityDetailDeadDateStatic, activityDetailDeadTimeStatic, activityDetailPosterImageStatic;

    String activityDetailTitleText, activityDetailTypeText, activityDetailDescriptionText, activityDetailStartdateText, activityDetailStarttimeText, activityDetailEnddateText, activityDetailEndtimeText, activityDetailVenueText, activityDetailTargetGroupText, activityDetailQuotaText, activityDetailFeeText, activityDetailRegMethodText, activityDetailDeadDateText, activityDetailDeadTimeText, activityDetailPosterImageText ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        activityDetailBtn = findViewById(R.id.activitydetails_btn);

        activityDetailTitle = findViewById(R.id.activitydetails_title_show);
        activityDetailType = findViewById(R.id.activitydetails_type_show);
        activityDetailDescription = findViewById(R.id.activitydetails_description_show);
        activityDetailStartdate = findViewById(R.id.activitydetails_startdate_show);
        activityDetailStarttime = findViewById(R.id.activitydetails_starttime_show);
        activityDetailEnddate = findViewById(R.id.activitydetails_enddate_show);
        activityDetailEndtime = findViewById(R.id.activitydetails_endtime_show);
        activityDetailVenue = findViewById(R.id.activitydetails_venue_show);
        activityDetailTargetGroup = findViewById(R.id.activitydetails_targetgroup_show);
        activityDetailQuota = findViewById(R.id.activitydetails_quota_show);
        activityDetailFee = findViewById(R.id.activitydetails_fee_show);
        activityDetailRegMethod = findViewById(R.id.activitydetails_regmethod_show);
        activityDetailDeadDate = findViewById(R.id.activitydetails_deaddate_show);
        activityDetailDeadTime = findViewById(R.id.activitydetails_deadtime_show);
        activityDetailPosterImage = findViewById(R.id.activitydetails_poster_image_show);


        activityDetailTypeStatic = findViewById(R.id.activitydetails_type);
        activityDetailDescriptionStatic = findViewById(R.id.activitydetails_description);
        activityDetailStartdateStatic = findViewById(R.id.activitydetails_startdate);
        activityDetailStarttimeStatic = findViewById(R.id.activitydetails_starttime);
        activityDetailEnddateStatic = findViewById(R.id.activitydetails_enddate);
        activityDetailEndtimeStatic = findViewById(R.id.activitydetails_endtime);
        activityDetailVenueStatic = findViewById(R.id.activitydetails_venue);
        activityDetailTargetGroupStatic = findViewById(R.id.activitydetails_targetgroup);
        activityDetailQuotaStatic = findViewById(R.id.activitydetails_quota);
        activityDetailFeeStatic = findViewById(R.id.activitydetails_fee);
        activityDetailRegMethodStatic = findViewById(R.id.activitydetails_regmethod);
        activityDetailDeadDateStatic = findViewById(R.id.activitydetails_deaddate);
        activityDetailDeadTimeStatic = findViewById(R.id.activitydetails_deadtime);
        activityDetailPosterImageStatic = findViewById(R.id.activitydetails_poster_image);


        activityDetailTitleText = getIntent().getStringExtra("title");
        activityDetailTypeText = getIntent().getStringExtra("type");
        activityDetailDescriptionText = getIntent().getStringExtra("description");
        activityDetailStartdateText = getIntent().getStringExtra("start date");
        activityDetailStarttimeText = getIntent().getStringExtra("start time");
        activityDetailEnddateText = getIntent().getStringExtra("end date");
        activityDetailEndtimeText = getIntent().getStringExtra("end time");
        activityDetailVenueText = getIntent().getStringExtra("venue");
        activityDetailTargetGroupText = getIntent().getStringExtra("target group");
        activityDetailQuotaText = getIntent().getStringExtra("quota");
        activityDetailFeeText = getIntent().getStringExtra("fee");
        activityDetailRegMethodText = getIntent().getStringExtra("registration link");
        activityDetailDeadDateText = getIntent().getStringExtra("date of deadline");
        activityDetailDeadTimeText = getIntent().getStringExtra("time of deadline");
        activityDetailPosterImageText = getIntent().getStringExtra("poster");


        activityDetailTitle.setText(activityDetailTitleText);
        activityDetailType.setText(activityDetailTypeText);
        activityDetailDescription.setText(activityDetailDescriptionText);
        activityDetailStartdate.setText(activityDetailStartdateText);
        activityDetailStarttime.setText(activityDetailStarttimeText);
        activityDetailEnddate.setText(activityDetailEnddateText);
        activityDetailEndtime.setText(activityDetailEndtimeText);
        activityDetailVenue.setText(activityDetailVenueText);
        activityDetailTargetGroup.setText(activityDetailTargetGroupText);
        activityDetailQuota.setText(activityDetailQuotaText);
        activityDetailFee.setText(activityDetailFeeText);
        activityDetailRegMethod.setText(activityDetailRegMethodText);

        activityDetailRegMethod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = activityDetailRegMethodText;
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                v.getContext().startActivity(i);

            }
        });

        activityDetailPosterImage.setText(activityDetailPosterImageText);
        activityDetailPosterImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = activityDetailPosterImageText;
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                v.getContext().startActivity(i);

            }
        });


        activityDetailDeadDate.setText(activityDetailDeadDateText);
        activityDetailDeadTime.setText(activityDetailDeadTimeText);

//        Glide.with(context).load(activityDetailPosterImageText).into(activityDetailPosterImage);

        activityDetailBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }




}
