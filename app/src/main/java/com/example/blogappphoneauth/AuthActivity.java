package com.example.blogappphoneauth;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import org.w3c.dom.Text;

import java.util.concurrent.TimeUnit;

public class AuthActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;

    private LinearLayout mPhoneLayout;
    private LinearLayout mCodeLayout;

    public static EditText mPhoneNumber;
    private EditText mCodeNumber;

    private ProgressBar mPhoneProgress;
    private ProgressBar mCodeProgress;

    private TextView mCodeText;

    private TextView mErrorText;

    private Button mSendCodeBtn;

    private  int btnType = 0;

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;

    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        mAuth = FirebaseAuth.getInstance();

        mPhoneLayout = (LinearLayout) findViewById(R.id.phone_layout);
        mCodeLayout = (LinearLayout) findViewById(R.id.code_layout);

        mPhoneNumber = (EditText)findViewById(R.id.phone_number);
        mCodeNumber = (EditText)findViewById(R.id.code_number);

        mPhoneProgress = (ProgressBar)findViewById(R.id.phone_progress);
        mCodeProgress = (ProgressBar)findViewById(R.id.code_progress);

        mCodeText = (TextView)findViewById(R.id.code_text);
        mErrorText = (TextView)findViewById(R.id.error_text);

        mSendCodeBtn = (Button) findViewById(R.id.send_code_btn);



        mSendCodeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (btnType == 0){
                mPhoneProgress.setVisibility(View.VISIBLE);
                mPhoneNumber.setEnabled(false);
                mSendCodeBtn.setEnabled(false);

                String phoneNumber = mPhoneNumber.getText().toString();

                PhoneAuthProvider.getInstance().verifyPhoneNumber(
                        phoneNumber,
                        60,
                        TimeUnit.SECONDS,
                        AuthActivity.this,
                        mCallbacks
                );
            } else {
                    mSendCodeBtn.setEnabled(false);
                    mCodeProgress.setVisibility(View.VISIBLE);

                    String verificationCode = mCodeNumber.getText().toString();
                    PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, verificationCode);
                    signInWithPhoneAuthCredential(credential);
                }
                }
            });

        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken token) {
                mVerificationId = verificationId;
                mResendToken = token;

                btnType = 1;

                mPhoneProgress.setVisibility(View.INVISIBLE);
                mCodeLayout.setVisibility(View.VISIBLE);

                mSendCodeBtn.setText("Verify Code");
                mSendCodeBtn.setEnabled(true);
            }
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

                signInWithPhoneAuthCredential (phoneAuthCredential);

            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                mErrorText.setText("There was some error in verification");
                mErrorText.setVisibility(View.VISIBLE);
            }


        };

        }
        private void signInWithPhoneAuthCredential (PhoneAuthCredential credential){
            mAuth.signInWithCredential(credential)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                FirebaseUser user = task.getResult().getUser();
                                Toast.makeText(AuthActivity.this, "You have verified your mobile phone number successfully!", Toast.LENGTH_LONG).show();
                                Intent PostIntent = new Intent(AuthActivity.this, PostActivity.class);
                                startActivity(PostIntent);
                                finish();
                            } else {
                                mErrorText.setText("There was some error in verifying the phone no.");
                                mErrorText.setVisibility(View.VISIBLE);
                            }
                        }
                    });
        }
        }




