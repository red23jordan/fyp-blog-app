package com.example.blogappphoneauth;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentTypeActivity extends Fragment {

    ImageView academicView, artView, careerView, collegeView, socialServiceView, societyView;

    FrameLayout activityTypeFrame;

    public FragmentTypeActivity() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment_type, container, false);

        activityTypeFrame = view.findViewById(R.id.activity_type_frame);
        academicView = view.findViewById(R.id.academic_view);
        academicView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFragment("academic");
            }
        });
        artView = view.findViewById(R.id.art_view);
        artView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFragment("art");
            }
        });
        careerView = view.findViewById(R.id.career_view);
        careerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFragment("career");
            }
        });
        collegeView = view.findViewById(R.id.college_view);
        collegeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFragment("college");
            }
        });
        socialServiceView = view.findViewById(R.id.social_service_view);
        socialServiceView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFragment("social_service");
            }
        });
        societyView = view.findViewById(R.id.society_view);
        societyView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFragment("society");
            }
        });

        return view;
    }
    private void setFragment(String type) {
        //Put type in bundle
        Bundle bundle = new Bundle();
        bundle.putString("type", type);
        //Create a new fragment
        ActivityFragment activityFragment = new ActivityFragment();
        //Set the bundle into fragment
        activityFragment.setArguments(bundle);

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.activity_type_frame, activityFragment);
        fragmentTransaction.commit();
    }

}
