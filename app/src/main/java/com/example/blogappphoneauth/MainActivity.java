package com.example.blogappphoneauth;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.example.blogappphoneauth.routeplanner.RoutePlannerActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private Toolbar mainToolbar;

    private BottomNavigationView mMainNav;
    private FrameLayout mMainFrame;

    private ActivityFragment todayFragment;
    private FragmentTypeActivity typeFragment;

    private FloatingActionButton addPostBtn;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();

       mainToolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(mainToolbar);
        getSupportActionBar().setTitle("ComeUni");

        mMainFrame = (FrameLayout) findViewById(R.id.main_frame);
        mMainNav = (BottomNavigationView) findViewById(R.id.main_nav);

        setFragment(new ActivityFragment());

        mMainNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()){
                    case R.id.nav_today :
                        setFragment(new ActivityFragment());
                        return true;

                    case R.id.nav_type :
                        setFragment(new FragmentTypeActivity());
                        return true;

                    default:
                        return false;

                }
            }
        });

        addPostBtn = findViewById(R.id.addPostbtn);
        addPostBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent AuthIntent = new Intent(MainActivity.this, AuthActivity.class);
                startActivity(AuthIntent);
                finish();

            }
        });

    }

    private void setFragment(Fragment fragment) {

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_frame, fragment);
        fragmentTransaction.commit();
    }

    // navigate to the account setting when the user is first logined
   /* @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser == null) {
            sendToSetup();
        }
    }*/

    // main_menu bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main_menu, menu);

        return true;
    }

    //// main_menu bar
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case R.id.action_logout_btn:
                /*logout();*/
            return true;

            case R.id.action_account_settings:
//                Intent settingsIntent = new Intent(MainActivity.this, SetupActivity.class);
//                startActivity(settingsIntent);
                Intent intent = new Intent(MainActivity.this, RoutePlannerActivity.class);
                MainActivity.this.startActivity(intent);
                return true;

            default:
                return false;
        }
    }
    /*private void logout() {

        mAuth.signOut();
        sendToAuth();
    }*/

    private void sendToSetup(){
        Intent SetupIntent = new Intent(MainActivity.this, SetupActivity.class);
        startActivity(SetupIntent);
        finish();
    }

}



