package com.example.blogappphoneauth;

import android.app.Application;
import android.content.Context;

//import androidx.multidex.MultiDex;
//import androidx.multidex.MultiDexApplication;

public class App extends Application {
    //you can leave this empty or override methods if you like so the thing is that you need to extend from MultiDexApplication
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        //MultiDex.install(this);
    }
}
