package com.example.blogappphoneauth;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.core.OrderBy;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import static androidx.constraintlayout.widget.Constraints.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentTodayActivity extends Fragment{

    private String type;
    private RecyclerView activityshow_list_view;
    public List<ActivityShowPost> activityshow_list;

    private FirebaseFirestore firebaseFirestore;
    private ActivityShowRecyclerAdapter activityShowRecyclerAdapter;


    public FragmentTodayActivity() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_fragment_today, container, false);
        //TODO get type from activity
        //......
        activityshow_list = new ArrayList<>();
        activityshow_list_view = view.findViewById(R.id.activityshow_list_view);

        activityShowRecyclerAdapter = new ActivityShowRecyclerAdapter(activityshow_list);
        activityshow_list_view.setLayoutManager(new LinearLayoutManager(container.getContext()));
        activityshow_list_view.setAdapter(activityShowRecyclerAdapter);


        firebaseFirestore = FirebaseFirestore.getInstance();

        //Query firstQuery = firebaseFirestore.collection("Activity Upload").orderBy("start_date").orderBy("start_time");
        Query firstQuery = firebaseFirestore.collection("Activity Upload");
        firstQuery.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent (QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {

                if (e != null) {
                    Log.d(TAG, "Error123:" + e.getMessage());
                } else {
                    List<ActivityShowPost> postList = new ArrayList<>();
                    for (DocumentChange doc : documentSnapshots.getDocumentChanges()) {
                        if (doc.getType() == DocumentChange.Type.ADDED) {
                            ActivityShowPost activityShowPost = doc.getDocument().toObject(ActivityShowPost.class);
                            Log.d(TAG, "ADDED " + activityShowPost.getFirst_name());
                            postList.add(activityShowPost);
                        }
                    }
                    activityShowRecyclerAdapter = new ActivityShowRecyclerAdapter(postList, type);
                    activityshow_list_view.setAdapter(activityShowRecyclerAdapter);
                    activityShowRecyclerAdapter.notifyDataSetChanged();
                }
            }});

        // Inflate the layout for this fragment
        return view;


    }

}
