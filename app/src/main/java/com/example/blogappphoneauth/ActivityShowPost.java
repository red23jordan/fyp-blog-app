package com.example.blogappphoneauth;

public class ActivityShowPost {

    public String last_name, first_name, users_id, email, type, title, description, start_date, start_time, end_date, end_time, venue, target_group, quota, fee, reg_method, dead_date, dead_time, image_photo;

    public ActivityShowPost() {}

    public ActivityShowPost(String last_name, String first_name, String users_id, String email, String type, String title, String description, String start_date, String start_time, String end_date, String end_time, String venue, String target_group, String quota, String fee, String reg_method, String dead_date, String dead_time, String image_photo) {
        this.last_name = last_name;
        this.first_name = first_name;
        this.users_id = users_id;
        this.email = email;
        this.type = type;
        this.title = title;
        this.description = description;
        this.start_date = start_date;
        this.start_time = start_time;
        this.end_date = end_date;
        this.end_time = end_time;
        this.venue = venue;
        this.target_group = target_group;
        this.quota = quota;
        this.fee = fee;
        this.reg_method = reg_method;
        this.dead_date = dead_date;
        this.dead_time = dead_time;
        this.image_photo = image_photo;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getUsers_id() {
        return users_id;
    }

    public void setUsers_id(String users_id) {
        this.users_id = users_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getTarget_group() {
        return target_group;
    }

    public void setTarget_group(String target_group) {
        this.target_group = target_group;
    }

    public String getQuota() {
        return quota;
    }

    public void setQuota(String quota) {
        this.quota = quota;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getReg_method() {
        return reg_method;
    }

    public void setReg_method(String reg_method) {
        this.reg_method = reg_method;
    }

    public String getDead_date() {
        return dead_date;
    }

    public void setDead_date(String dead_date) {
        this.dead_date = dead_date;
    }

    public String getDead_time() {
        return dead_time;
    }

    public void setDead_time(String dead_time) {
        this.dead_time = dead_time;
    }

    public String getImage_photo() {
        return image_photo;
    }

    public void setImage_photo(String image_photo) {
        this.image_photo = image_photo;
    }
}
