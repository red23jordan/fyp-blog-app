package com.example.blogappphoneauth;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class ActivityFragment extends Fragment{

    private String type = "all";
    private RecyclerView activityshow_list_view;

    private FirebaseFirestore firebaseFirestore;
    private ActivityShowRecyclerAdapter activityShowRecyclerAdapter;

    public ActivityFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            type = bundle.getString("type");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fragment_today, container, false);
        activityshow_list_view = view.findViewById(R.id.activityshow_list_view);
        activityshow_list_view.setLayoutManager(new LinearLayoutManager(container.getContext()));

        firebaseFirestore = FirebaseFirestore.getInstance();

        firebaseFirestore.collection("Activity Upload")
                .orderBy("start_date")
                .orderBy("start_time")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(task.isSuccessful()) {
                            List<ActivityShowPost> postList = new ArrayList<>();
                            for(QueryDocumentSnapshot doc : task.getResult()) {
                                ActivityShowPost activityShowPost = doc.toObject(ActivityShowPost.class);
                                postList.add(activityShowPost);
                            }
                            displayList(postList);
                        }
                    }
                });

        // Inflate the layout for this fragment
        return view;
    }

    private void displayList(List<ActivityShowPost> postList) {
        activityShowRecyclerAdapter = new ActivityShowRecyclerAdapter(postList, type);
        activityshow_list_view.setAdapter(activityShowRecyclerAdapter);
        activityShowRecyclerAdapter.notifyDataSetChanged();
    }

}
