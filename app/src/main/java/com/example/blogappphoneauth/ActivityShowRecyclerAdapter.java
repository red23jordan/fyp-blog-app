package com.example.blogappphoneauth;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ActivityShowRecyclerAdapter extends RecyclerView.Adapter<ActivityShowRecyclerAdapter.ViewHolder> {

    private List <ActivityShowPost> result_list;
    public Context context;

    public ActivityShowRecyclerAdapter(List <ActivityShowPost> activityshow_list){
        this.result_list = activityshow_list;
    }

    public ActivityShowRecyclerAdapter(List <ActivityShowPost> activityshow_list, String type){
        if (type.equalsIgnoreCase("all")) {
            this.result_list = activityshow_list;
        } else {
            this.result_list = filterByType(activityshow_list, type);
        }
    }

    private List <ActivityShowPost> filterByType(List <ActivityShowPost> activityshow_list, String type) {
        List <ActivityShowPost> newList = new ArrayList<>();
        for(int i=0; i<activityshow_list.size(); i++) {
            ActivityShowPost activityShowPost = activityshow_list.get(i);
            Log.d("ActivityShowAdapter", activityShowPost.getImage_photo());
            if(activityShowPost.getType().equalsIgnoreCase(type)) {
                newList.add(activityShowPost);
            }
        }
        return newList;
    }

    @Override
    public ActivityShowRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activityshow_list_item, parent, false);
        context = parent.getContext();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ActivityShowRecyclerAdapter.ViewHolder holder, final int position) {

        String title_data = result_list.get(position).getTitle();
        holder.setTitleText(title_data);

        String startdate_data = result_list.get(position).getStart_date();
        holder.setStartdateText(startdate_data);

        String starttime_data = result_list.get(position).getStart_time();
        holder.setStarttimeText(starttime_data );

        String description_data = result_list.get(position).getDescription();
        holder.setDescriptionText(description_data );

        String image_photo = result_list.get(position).getImage_photo();
        holder.setPosterImageText(image_photo);

        String regmethod_data = result_list.get(position).getReg_method();
        holder.setRegmethodText(regmethod_data);

        String activitytype_data = result_list.get(position).getType();
        holder.setActivityshowTypeText(activitytype_data);



        holder.constraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailsActivity.class);
                intent.putExtra("title", result_list.get(position).getTitle());
                intent.putExtra("type", result_list.get(position).getType());
                intent.putExtra("description", result_list.get(position).getDescription());
                intent.putExtra("start date", result_list.get(position).getStart_date());
                intent.putExtra("start time", result_list.get(position).getStart_time());
                intent.putExtra("end date", result_list.get(position).getEnd_date());
                intent.putExtra("end time", result_list.get(position).getEnd_time());
                intent.putExtra("venue", result_list.get(position).getVenue());
                intent.putExtra("target group", result_list.get(position).getTarget_group());
                intent.putExtra("quota", result_list.get(position).getQuota());
                intent.putExtra("fee", result_list.get(position).getFee());
                intent.putExtra("registration link", result_list.get(position).getReg_method());
                intent.putExtra("date of deadline", result_list.get(position).getDead_date());
                intent.putExtra("time of deadline", result_list.get(position).getDead_time());
                intent.putExtra("poster", result_list.get(position).getImage_photo());
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return result_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
//in the list item
        private View mView;
        private TextView titleView;
        private TextView startdateView;
        private TextView starttimeView;
        private TextView descriptionView;
        private ImageView posterImageView;
        private TextView regmethodView;
        private CircleImageView activityshowTypeView;

        private ConstraintLayout constraintLayout;





        public ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            constraintLayout = mView.findViewById(R.id.activityshow_constraintlayout);

        }

        public void setTitleText (String titleText) {
            titleView = mView.findViewById(R.id.activityshow_title);
            titleView.setText(titleText);
        }

        public void setStarttimeText (String starttimeText) {
            starttimeView = mView.findViewById(R.id.activityshow_time);
            starttimeView.setText(starttimeText);
        }

        public void setStartdateText (String startdateText) {
            startdateView= mView.findViewById(R.id.activityshow_date);
            startdateView.setText(startdateText);
        }

        public void setDescriptionText (String descriptionText) {
            descriptionView= mView.findViewById(R.id.activityshow_description);
            descriptionView.setText(descriptionText);
        }

        public void setPosterImageText (String downloadUri) {
            posterImageView = mView.findViewById(R.id.activityshow_poster_image);
            Glide.with(context).load(downloadUri).into(posterImageView);
        }

        public void setRegmethodText (final String regmethodText) {
            regmethodView = mView.findViewById(R.id.activityshow_reg_method);
            regmethodView.setText(regmethodText);
            regmethodView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = regmethodText;
                    Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    v.getContext().startActivity(i);

                }
            });
        }

        public void setActivityshowTypeText (String activityshowTypeText) {
            activityshowTypeView = mView.findViewById(R.id.activityshow_type_logo);
            String acamdeic = "Academic";
            String art = "Art";
            String career = "Career";
            String college = "College";
            String social_service = "Social Services";
            String society = "Society";
            if (acamdeic.compareTo(activityshowTypeText) == 0) {
                Drawable myDrawable = context.getResources().getDrawable(R.drawable.academic);
                activityshowTypeView.setImageDrawable(myDrawable);
            }

            else if (art.compareTo(activityshowTypeText) == 0) {
                Drawable myDrawable = context.getResources().getDrawable(R.drawable.art);
                activityshowTypeView.setImageDrawable(myDrawable);
            }

            else if (career.compareTo(activityshowTypeText) == 0) {
                Drawable myDrawable = context.getResources().getDrawable(R.drawable.career);
                activityshowTypeView.setImageDrawable(myDrawable);
            }

            else if (college.compareTo(activityshowTypeText) == 0) {
                Drawable myDrawable = context.getResources().getDrawable(R.drawable.college);
                activityshowTypeView.setImageDrawable(myDrawable);
            }

            else if (social_service.compareTo(activityshowTypeText) == 0) {
                Drawable myDrawable = context.getResources().getDrawable(R.drawable.socialservices);
                activityshowTypeView.setImageDrawable(myDrawable);
            }

            else if (society.compareTo(activityshowTypeText) == 0) {
                Drawable myDrawable = context.getResources().getDrawable(R.drawable.society);
                activityshowTypeView.setImageDrawable(myDrawable);
            }

        }





    }



}
