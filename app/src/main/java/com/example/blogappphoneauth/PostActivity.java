package com.example.blogappphoneauth;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.w3c.dom.Text;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class PostActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private Toolbar newPostToolbar;
    private TextView PersonalDetails;
    private EditText LastName;
    private EditText FirstName;
    private EditText EmailAddress;
    private TextView ActivityDetails;
    private TextView ActivityType;
    public String activityTypeChosen;
    private EditText Title;
    private EditText Description;
    private EditText Venue;
    private EditText TargetGroup;
    private EditText Quota;
    private EditText Fee;
    private EditText RegMethod;
    private TextView DeadlineofReg;


    private static final String TAG = "PostActivity";

    private TextView mStart;
    private TextView mEnd;

    private TextView mShowStartingDate;
    private TextView mEditStartingDate;
    private TextView mShowEndingDate;
    private TextView mEditEndingDate;
    private TextView mShowDeadDate;
    private TextView mEditDeadDate;
    private DatePickerDialog.OnDateSetListener mDateSetListener1;
    private DatePickerDialog.OnDateSetListener mDateSetListener2;
    private DatePickerDialog.OnDateSetListener mDateSetListener3;

    private TextView mShowStartingTime;
    private TextView mEditStartingTime;
    private TextView mShowEndingTime;
    private TextView mEditEndingTime;
    private TextView mShowDeadTime;
    private TextView mEditDeadTime;
    TimePickerDialog timePickerDialog;
    Calendar calendar;
    int currentHour;
    int currentMinute;
    String amPm;

    private Button submitBtn;
    private ProgressBar posterProgress;

    private ImageView posterImage;
    private Uri posterImageURI = null;


    private StorageReference storageReference;
    private FirebaseAuth firebaseAuth;
    private FirebaseFirestore firebaseFirestore;

    UploadTask uploadTask;

    String phoneNumber = AuthActivity.mPhoneNumber.getText().toString();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        /*newPostToolbar = findViewById(R.id.newpost_toolbar);
        setSupportActionBar(newPostToolbar);
        getSupportActionBar().setTitle("Add Activity");
*/
        PersonalDetails = (TextView) findViewById(R.id.personal_details);
        LastName = (EditText) findViewById(R.id.last_name_editText);
        FirstName = (EditText) findViewById(R.id.first_name_editText);
        EmailAddress = (EditText) findViewById(R.id.email_editText);
        ActivityDetails = (TextView) findViewById(R.id.activity_details);
        ActivityType = (TextView) findViewById(R.id.activity_type);
        Title = (EditText) findViewById(R.id.title_editText);
        Description = (EditText) findViewById(R.id.description_editText);
        Venue = (EditText) findViewById(R.id.venue_editText);
        TargetGroup= (EditText) findViewById(R.id.targetgroup_editText);
        Quota= (EditText) findViewById(R.id.quota_editText);
        Fee = (EditText) findViewById(R.id.fee_editText);
        RegMethod= (EditText) findViewById(R.id.regmethod_editText);
        DeadlineofReg= (TextView) findViewById(R.id.activity_deadlineofreg);



        final Spinner spinner = (Spinner) findViewById(R.id.action_bar_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.ActivityType, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        mStart = (TextView) findViewById(R.id.activity_start);
        mEnd = (TextView) findViewById(R.id.activity_end);

        mShowStartingDate = (TextView) findViewById(R.id.show_start_date);
        mShowStartingTime = (TextView) findViewById(R.id.show_start_time);
        mShowEndingDate = (TextView) findViewById(R.id.show_end_date);
        mShowEndingTime = (TextView) findViewById(R.id.show_end_time);
        mShowDeadDate = (TextView) findViewById(R.id.show_dead_date);
        mShowDeadTime = (TextView) findViewById(R.id.show_dead_time);

        mEditStartingDate = (TextView) findViewById(R.id.edit_start_date);
        mEditStartingDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        PostActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener1,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });
        mDateSetListener1 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month += 1;
                Log.d(TAG, "onDateSet: Date: " + year + "/" + month + "/" + dayOfMonth);
//                SimpleDateFormat date = new SimpleDateFormat("yyyy/MM/dd");
                String date = year + "/" + month + "/" + dayOfMonth;
                SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
                String date1 = format.format(Date.parse(date));
                mEditStartingDate.setText(date1);


            }
        };

        mEditEndingDate = (TextView) findViewById(R.id.edit_end_date);
        mEditEndingDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        PostActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener2,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });
        mDateSetListener2 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month += 1;
                Log.d(TAG, "onDateSet: Date: " + year + "/" + month + "/" + dayOfMonth);
                String date = year + "/" + month + "/" + dayOfMonth;
                SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
                String date2 = format.format(Date.parse(date));
                mEditEndingDate.setText(date2);
            }
        };

        mEditDeadDate = (TextView) findViewById(R.id.edit_dead_date);
        mEditDeadDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        PostActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener3,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });
        mDateSetListener3 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month += 1;
                Log.d(TAG, "onDateSet: Date: " + year + "/" + month + "/" + dayOfMonth);
                String date = year + "/" + month + "/" + dayOfMonth;
                SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
                String date3 = format.format(Date.parse(date));
                mEditDeadDate.setText(date3);
            }
        };

        mEditStartingTime = (TextView) findViewById(R.id.edit_start_time);
        mEditStartingTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar = Calendar.getInstance();
                currentHour = calendar.get(Calendar.HOUR_OF_DAY);
                currentMinute = calendar.get(Calendar.MINUTE);

                timePickerDialog = new TimePickerDialog(PostActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        if (hourOfDay >= 12) {
                            amPm = "PM";
                        } else {
                            amPm = "AM";
                        }
                        mEditStartingTime.setText(String.format("%02d:%02d", hourOfDay, minute) + amPm);
                    }
                }, currentHour, currentMinute, false);
                timePickerDialog.show();
            }
        });

        mEditEndingTime = (TextView) findViewById(R.id.edit_end_time);
        mEditEndingTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar = Calendar.getInstance();
                currentHour = calendar.get(Calendar.HOUR_OF_DAY);
                currentMinute = calendar.get(Calendar.MINUTE);

                timePickerDialog = new TimePickerDialog(PostActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        if (hourOfDay >= 12) {
                            amPm = "PM";
                        } else {
                            amPm = "AM";
                        }
                        mEditEndingTime.setText(String.format("%02d:%02d", hourOfDay, minute) + amPm);
                    }
                }, currentHour, currentMinute, false);
                timePickerDialog.show();
            }
        });

        mEditDeadTime = (TextView) findViewById(R.id.edit_dead_time);
        mEditDeadTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar = Calendar.getInstance();
                currentHour = calendar.get(Calendar.HOUR_OF_DAY);
                currentMinute = calendar.get(Calendar.MINUTE);

                timePickerDialog = new TimePickerDialog(PostActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        if (hourOfDay >= 12) {
                            amPm = "PM";
                        } else {
                            amPm = "AM";
                        }
                        mEditDeadTime.setText(String.format("%02d:%02d", hourOfDay, minute) + amPm);
                    }
                }, currentHour, currentMinute, false);
                timePickerDialog.show();
            }
        });

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();



        posterImage = (ImageView) findViewById(R.id.newpost_add_activity_default_logo);
        submitBtn = (Button) findViewById(R.id.activity_submit);
        posterProgress = (ProgressBar) findViewById(R.id.upload_activity_progress);


        posterImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                    if(ContextCompat.checkSelfPermission(PostActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                        Toast.makeText(PostActivity.this, "Permission Denied", Toast.LENGTH_LONG).show();
                        ActivityCompat.requestPermissions(PostActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                    }
                    else {
                        // start picker to get image for cropping and then use the image in cropping activity
                        CropImage.activity()
                                .setGuidelines(CropImageView.Guidelines.ON)
                                .setMinCropResultSize(512,512)
                                .setAspectRatio(2,3)
                                .start(PostActivity.this);
                    }
                }
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String activityLastName = LastName.getText().toString();
                final String activityFirstName = FirstName.getText().toString();
                final String activityEmail = EmailAddress.getText().toString();
                final String activityCategory = activityTypeChosen;
                final String activityTitle = Title.getText().toString();
                final String activityDescription = Description.getText().toString();
                final String activityStartingDate = mEditStartingDate.getText().toString();
                final String activityStartingTime = mEditStartingTime.getText().toString();
                final String activityEndingDate = mEditEndingDate.getText().toString();
                final String activityEndingTime = mEditEndingTime.getText().toString();
                final String activityVenue = Venue.getText().toString();
                final String activityTargetGroup = TargetGroup.getText().toString();
                final String activityQuota = Quota.getText().toString();
                final String activityFee = Fee.getText().toString();
                final String activityRegMethod = RegMethod.getText().toString();
                final String activityDeadDate = mEditDeadDate.getText().toString();
                final String activityDeadTime = mEditDeadTime.getText().toString();




                if (!TextUtils.isEmpty(activityLastName) && !TextUtils.isEmpty(activityFirstName) && !TextUtils.isEmpty(activityTitle) && !TextUtils.isEmpty(activityStartingDate) && !TextUtils.isEmpty(activityStartingTime) && !TextUtils.isEmpty(activityEndingDate) && !TextUtils.isEmpty(activityEndingTime) && !TextUtils.isEmpty(activityDeadDate) && !TextUtils.isEmpty(activityDeadTime) && posterImageURI!=null) {

                    if (activityEndingDate.compareTo(activityStartingDate) < 0) {
                        Toast.makeText(PostActivity.this, "End date is earlier than start date. Fix it please.", Toast.LENGTH_LONG).show();}
                    else if ((activityEndingTime.compareTo(activityStartingTime) < 0) && (activityEndingDate.compareTo(activityStartingDate) <= 0)) {
                        Toast.makeText(PostActivity.this, "End time is earlier than start time. Fix it please.", Toast.LENGTH_LONG).show();}
                    else if (activityDeadDate.compareTo(activityStartingDate) > 0) {
                        Toast.makeText(PostActivity.this, "Start date is earlier than date of deadline. Fix it please.", Toast.LENGTH_LONG).show();}
                    else if ((activityDeadTime.compareTo(activityStartingTime) > 0) && (activityDeadDate.compareTo(activityStartingDate) >= 0)) {
                        Toast.makeText(PostActivity.this, "Start time is earlier than time of deadline. Fix it please.", Toast.LENGTH_LONG).show();}
                    else{
                        final String user_id = firebaseAuth.getCurrentUser().getUid();
                        posterProgress.setVisibility(View.VISIBLE);


                        final String randomName = FieldValue.serverTimestamp().toString();

                        final StorageReference image_path = storageReference.child("poster_images").child(activityTitle + ".jpg");

                        uploadTask = image_path.putFile(posterImageURI);
                        Task<Uri> uriTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                            @Override
                            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                                if (!task.isSuccessful()) {
                                    throw task.getException();
                                }
                                return image_path.getDownloadUrl();
                            }
                        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                            @Override
                            public void onComplete(@NonNull Task<Uri> task) {
                                if (task.isSuccessful()) {
                                    Uri downloadUri = task.getResult();


                                    Map<String, String> posterMap = new HashMap<>();
                                    posterMap.put("last_name", activityLastName);
                                    posterMap.put("first_name", activityFirstName);
                                    posterMap.put("users_id", user_id);
                                    posterMap.put("email", activityEmail);
                                    //                               posterMap.put("timestamp", randomName);
                                    posterMap.put("type", activityCategory);
                                    posterMap.put("title", activityTitle);
                                    posterMap.put("description", activityDescription);
                                    posterMap.put("start_date", activityStartingDate);
                                    posterMap.put("start_time", activityStartingTime);
                                    posterMap.put("end_date", activityEndingDate);
                                    posterMap.put("end_time", activityEndingTime);
                                    posterMap.put("venue", activityVenue);
                                    posterMap.put("target_group", activityTargetGroup);
                                    posterMap.put("quota", activityQuota);
                                    posterMap.put("fee", activityFee);
                                    posterMap.put("reg_method", activityRegMethod);
                                    posterMap.put("dead_date", activityDeadDate);
                                    posterMap.put("dead_time", activityDeadTime);
                                    posterMap.put("image_photo", downloadUri.toString());

                                    firebaseFirestore.collection("Activity Upload").add(posterMap).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                                        @Override
                                        public void onComplete(@NonNull Task<DocumentReference> task) {
                                            if (task.isSuccessful()) {

                                                Toast.makeText(PostActivity.this, "The poster is uploaded.", Toast.LENGTH_LONG).show();
                                                Intent mainIntent = new Intent(PostActivity.this, MainActivity.class);
                                                startActivity(mainIntent);
                                                finish();

                                            } else {

                                                String error = task.getException().getMessage();
                                                Toast.makeText(PostActivity.this, "(FIRESTORE Error) : " + error, Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    });

                                } else {
                                    String error = task.getException().getMessage();
                                    Toast.makeText(PostActivity.this, "(IMAGE Error) :" + error, Toast.LENGTH_LONG).show();
                                }

                                posterProgress.setVisibility(View.INVISIBLE);

                            }
                        });
                    }
                }else {
                    Toast.makeText(PostActivity.this, "You haven't filled in all the required field", Toast.LENGTH_LONG).show();
                }}
        });



    }



    /* select the activity type*/
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
         activityTypeChosen = parent.getItemAtPosition(position).toString();
 //      Toast.makeText(parent.getContext(), activityTypeChosen, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    /*set the uri to the image + crop image*/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
              posterImageURI = result.getUri();
              posterImage.setImageURI(posterImageURI);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }
}
